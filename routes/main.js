const express = require('express')
const controller = require('../controllers/main')
const router = express.Router()

router.get('/', controller.getHomePage)
router.post('/', controller.getHomePage)

module.exports = router
